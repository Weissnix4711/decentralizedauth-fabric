package net.decentralizedauth.fabric;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.fabricmc.api.ModInitializer;

public class DecentralizedAuth implements ModInitializer {
  public static final Logger LOGGER = LoggerFactory.getLogger("decentralizedauth");

  @Override
  public void onInitialize() {
    LOGGER.info("Decentralized Auth injected.");
    Security.addProvider(new BouncyCastleProvider());
  }
}
