package net.decentralizedauth.fabric.server;

import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthProofC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.minecraft.network.listener.ServerPacketListener;

public interface ServerDecentralizedAuthPacketListener
extends ServerPacketListener {
    public void onStart(DecentralizedAuthStartC2SPacket packet);

    public void onEncryptionResponse(DecentralizedAuthEncryptionResponseC2SPacket packet);

    public void onAuthProof(DecentralizedAuthAuthProofC2SPacket packet);

    public void onProfileResponse(DecentralizedAuthProfileResponseC2SPacket packet);
}
