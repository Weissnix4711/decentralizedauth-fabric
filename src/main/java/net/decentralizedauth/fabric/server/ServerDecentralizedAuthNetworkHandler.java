package net.decentralizedauth.fabric.server;

import java.security.KeyPair;
import java.util.Optional;
import java.util.UUID;

import javax.crypto.SecretKey;

import org.apache.commons.lang3.Validate;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import com.mojang.authlib.GameProfile;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftServerAccess;
import net.decentralizedauth.fabric.access.UserCacheAccess;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthChallengeS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthProofC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthCompressionS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthDisconnectS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthSuccessS2CPacket;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.decentralizedauth.fabric.util.UUIDv5;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.encryption.NetworkEncryptionException;
import net.minecraft.network.packet.s2c.play.DisconnectS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.TextifiedException;

public class ServerDecentralizedAuthNetworkHandler
implements ServerDecentralizedAuthPacketListener {
  static final Logger LOGGER = DecentralizedAuth.LOGGER;
  private static final int TIMEOUT_TICKS = 600;
  final MinecraftServer server;
  public final ClientConnection connection;
  State state = State.START;
  private int loginTicks;
  @Nullable
  GameProfile profile;
  @Nullable
  private ServerPlayerEntity delayedPlayer;
  private SecretKey sharedSecret;
  private final KeyPair ephemeralKeypair;

  public ServerDecentralizedAuthNetworkHandler(MinecraftServer server, ClientConnection connection) {
    this.server = server;
    this.connection = connection;
    this.ephemeralKeypair = DecentralizedAuthNetworkEncryptionUtils.generateX25519EphemeralKeypair();
  }

  public void acceptPlayer() {
    Text text;
    if ((text = this.server.getPlayerManager().checkCanJoin(this.connection.getAddress(), this.profile)) != null) {
      this.disconnect(text);
    } else {
      this.state = State.ACCEPTED;
      if (this.server.getNetworkCompressionThreshold() >= 0 && !this.connection.isLocal()) {
        this.connection.send(new DecentralizedAuthCompressionS2CPacket(this.server.getNetworkCompressionThreshold()), channelFuture -> this.connection.setCompressionThreshold(this.server.getNetworkCompressionThreshold(), true));
      }
      this.connection.send(new DecentralizedAuthSuccessS2CPacket(this.profile));
      ServerPlayerEntity serverPlayerEntity = this.server.getPlayerManager().getPlayer(this.profile.getId());
      try {
        ServerPlayerEntity serverPlayerEntity2 = this.server.getPlayerManager().createPlayer(this.profile, null);
        if (serverPlayerEntity != null) {
          this.state = State.DELAY_ACCEPT;
          this.delayedPlayer = serverPlayerEntity2;
        } else {
          this.addToServer(serverPlayerEntity2);
        }
      }
      catch (Exception exception) {
        LOGGER.error("Couldn't place player in world", exception);
        MutableText text2 = Text.translatable("multiplayer.disconnect.invalid_player_data");
        this.connection.send(new DisconnectS2CPacket(text2));
        this.connection.disconnect(text2);
      }
    }
  }

  private void addToServer(ServerPlayerEntity player) {
      this.server.getPlayerManager().onPlayerConnect(this.connection, player);
  }

  @Override
  public void onDisconnected(Text reason) {
    LOGGER.info("{} lost connection: {}", (Object)this.getConnectionInfo(), (Object)reason.getString());
  }

  public void tick() {
    ServerPlayerEntity serverPlayerEntity;
    if (this.state == State.READY_TO_ACCEPT) {
      this.acceptPlayer();
    } else if (this.state == State.DELAY_ACCEPT && (serverPlayerEntity = this.server.getPlayerManager().getPlayer(this.profile.getId())) == null) {
      this.state = State.READY_TO_ACCEPT;
      this.addToServer(this.delayedPlayer);
      this.delayedPlayer = null;
    }
    if (this.loginTicks++ == TIMEOUT_TICKS) {
      this.disconnect(Text.translatable("multiplayer.disconnect.slow_login"));
    }
  }

  public String getConnectionInfo() {
    if (this.profile != null) {
      return this.profile + " (" + this.connection.getAddress() + ")";
    }
    return String.valueOf(this.connection.getAddress());
  }

  @Override
  public ClientConnection getConnection() {
    return this.connection;
  }

  public void disconnect(Text reason) {
    try {
      LOGGER.info("Disconnecting {}: {}", (Object)this.getConnectionInfo(), (Object)reason.getString());
      this.connection.send(new DecentralizedAuthDisconnectS2CPacket(reason));
      this.connection.disconnect(reason);
    }
    catch (Exception exception) {
      LOGGER.error("Error whilst disconnecting player", exception);
    }
  }

  @Override
  public void onStart(DecentralizedAuthStartC2SPacket packet) {
    Validate.validState(this.state == State.START, "Unexpected start packet", new Object[0]);
    if (((MinecraftServerAccess) this.server).getDecentralizedAuthIdentity() == null) {
      this.disconnect(Text.of("Server is not yet ready to accept Decentralized Auth connections - try again shortly!"));
      return;
    }
    if (this.server.isOnlineMode() && !this.connection.isLocal()) {
      this.state = State.KEY;
      this.connection.send(new DecentralizedAuthEncryptionRequestS2CPacket(this.ephemeralKeypair.getPublic()));
    } else {
      this.state = State.READY_TO_ACCEPT;
    }
  }

  @Override
  public void onEncryptionResponse(DecentralizedAuthEncryptionResponseC2SPacket packet) {
    Validate.validState(this.state == State.KEY, "Unexpected key packet", new Object[0]);
    try {
      this.sharedSecret = DecentralizedAuthNetworkEncryptionUtils.finishX25519(this.ephemeralKeypair.getPrivate(), packet.getPublicKey());

      DecentralizedAuthNetworkEncryptionUtils.AESCiphers aesCiphers = DecentralizedAuthNetworkEncryptionUtils.deriveAESCiphers(this.sharedSecret);

      this.connection.setupEncryption(aesCiphers.decryptCipher(), aesCiphers.encryptCipher());
    }
    catch (NetworkEncryptionException e) {
      throw new IllegalStateException("Protocol error", e);
    }

    this.state = State.AUTHENTICATING;

    Ed25519PrivateKeyParameters serverIdentityPrivateKey = ((MinecraftServerAccess) this.server).getDecentralizedAuthIdentity();
    Ed25519PublicKeyParameters serverIdentityPublicKey = serverIdentityPrivateKey.generatePublicKey();

    byte[] signedSharedSecret = DecentralizedAuthNetworkEncryptionUtils.generateIdentityProof(serverIdentityPrivateKey, this.sharedSecret);

    this.connection.send(new DecentralizedAuthAuthChallengeS2CPacket(serverIdentityPublicKey, signedSharedSecret));
  }

  @Override
  public void onAuthProof(DecentralizedAuthAuthProofC2SPacket packet) {
    Validate.validState(this.state == State.AUTHENTICATING, "Unexpected auth proof packet", new Object[0]);

    Ed25519PublicKeyParameters clientIdentityPublicKey = packet.getClientIdentityPublicKey();
    if (!packet.verifySignedSharedSecret(this.sharedSecret)) {
      this.disconnect(Text.of("Failed to validate client identity!"));
      return;
    }

    final UUID playerUUID = UUIDv5.playerUUIDFromClientPubkey(clientIdentityPublicKey);

    // TODO check if client is allowed to join by UUID

    // TODO add a config option
    boolean allowRename = true;

    if (!allowRename) {
      Optional<GameProfile> previousLogin = this.server.getUserCache().getByUuid(playerUUID);

      // TODO in OfficialAccountPrecedence.STRICT mode, this should still check
      // that an official account hasn't renamed to the cached name
      if (previousLogin.isPresent()) {
        this.profile = previousLogin.get();
        this.state = State.READY_TO_ACCEPT;
        return;
      }
    }

    this.profile = new GameProfile(playerUUID, null);

    // TODO cache skin textures by UUID
    boolean requestSkinTexture = true;

    this.state = State.PROFILE;
    this.connection.send(new DecentralizedAuthProfileRequestS2CPacket(requestSkinTexture));
  }

  enum OfficialAccountPrecedence {
    STRICT,
    PREFER,
    NONE;
  }

  @Override
  public void onProfileResponse(DecentralizedAuthProfileResponseC2SPacket packet) {
    Validate.validState(this.state == State.PROFILE, "Unexpected profile packet", new Object[0]);

    // TODO add a config option
    OfficialAccountPrecedence precedence = OfficialAccountPrecedence.PREFER;

    Optional<GameProfile> sameNameLogin;

    switch (precedence) {
      case STRICT:
        // If the name isn't in the cache, this also checks Mojang for official
        // accounts.
        sameNameLogin = this.server.getUserCache().findByName(packet.name());
        break;
      // PREFER and NONE fall through: PREFER is only different from NONE in
      // that it will evict cache entries on an official account login.
      case PREFER:
        // TODO evict cache entries on official account logins
      case NONE:
      default:
        sameNameLogin = ((UserCacheAccess) this.server.getUserCache()).findCachedByName(packet.name());
        break;
    }

    if (sameNameLogin.isEmpty() || sameNameLogin.get().getId().equals(this.profile.getId())) {
      this.profile = new GameProfile(this.profile.getId(), packet.name());
    } else {
      this.disconnect(Text.of("Your requested username is already in use by another account. Please choose a different username."));
      return;
    }

    // TODO handle skin texture

    this.state = State.READY_TO_ACCEPT;
  }

  static enum State {
    START,
    KEY,
    AUTHENTICATING,
    PROFILE,
    READY_TO_ACCEPT,
    DELAY_ACCEPT,
    ACCEPTED;
  }

  static class LoginException
  extends TextifiedException {
    public LoginException(Text text) {
      super(text);
    }

    public LoginException(Text text, Throwable throwable) {
      super(text, throwable);
    }
  }
}
