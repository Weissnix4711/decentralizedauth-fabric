package net.decentralizedauth.fabric.client;

import java.security.KeyPair;
import java.util.Base64;
import java.util.function.Consumer;

import javax.crypto.SecretKey;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.jetbrains.annotations.Nullable;

import com.mojang.authlib.GameProfile;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftClientAccess;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthChallengeS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthProofC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthCompressionS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthDisconnectS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthSuccessS2CPacket;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.DisconnectedScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.realms.gui.screen.DisconnectedRealmsScreen;
import net.minecraft.client.realms.gui.screen.RealmsScreen;
import net.minecraft.client.util.NetworkUtils;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkState;
import net.minecraft.network.encryption.NetworkEncryptionException;
import net.minecraft.screen.ScreenTexts;
import net.minecraft.text.Text;

@Environment(value=EnvType.CLIENT)
public class ClientDecentralizedAuthNetworkHandler
implements ClientDecentralizedAuthPacketListener {
  private final MinecraftClient client;
  @Nullable
  private final Screen parentScreen;
  private final Consumer<Text> statusConsumer;
  private final ClientConnection connection;
  private GameProfile profile;
  private SecretKey sharedSecret;
  private final KeyPair ephemeralKeypair;

  public ClientDecentralizedAuthNetworkHandler(ClientConnection connection, MinecraftClient client, @Nullable Screen parentGui, Consumer<Text> statusConsumer) {
    this.connection = connection;
    this.client = client;
    this.parentScreen = parentGui;
    this.statusConsumer = statusConsumer;
    this.ephemeralKeypair = DecentralizedAuthNetworkEncryptionUtils.generateX25519EphemeralKeypair();
  }

  @Override
  public void onSuccess(DecentralizedAuthSuccessS2CPacket packet) {
    this.statusConsumer.accept(Text.translatable("connect.joining"));
    this.profile = packet.getProfile();
    this.connection.setState(NetworkState.PLAY);
    // Forced telemetry is shady, but there's no way to construct a
    // `ClientPlayNetworkHandler` without it. Recommended:
    // https://github.com/kb-1000/no-telemetry
    this.connection.setPacketListener(new ClientPlayNetworkHandler(this.client, this.parentScreen, this.connection, this.profile, this.client.createTelemetrySender()));
  }

  @Override
  public void onDisconnected(Text reason) {
    if (this.parentScreen != null && this.parentScreen instanceof RealmsScreen) {
      this.client.setScreen(new DisconnectedRealmsScreen(this.parentScreen, ScreenTexts.CONNECT_FAILED, reason));
    } else {
      this.client.setScreen(new DisconnectedScreen(this.parentScreen, ScreenTexts.CONNECT_FAILED, reason));
    }
  }

  @Override
  public ClientConnection getConnection() {
    return this.connection;
  }

  @Override
  public void onDisconnect(DecentralizedAuthDisconnectS2CPacket packet) {
    this.connection.disconnect(packet.getReason());
  }

  @Override
  public void onCompression(DecentralizedAuthCompressionS2CPacket packet) {
    if (!this.connection.isLocal()) {
      this.connection.setCompressionThreshold(packet.getCompressionThreshold(), false);
    }
  }

  @Override
  public void onEncryptionRequest(DecentralizedAuthEncryptionRequestS2CPacket packet) {
    DecentralizedAuthEncryptionResponseC2SPacket responsePacket;
    DecentralizedAuthNetworkEncryptionUtils.AESCiphers aesCiphers;
    try {
      this.sharedSecret = DecentralizedAuthNetworkEncryptionUtils.finishX25519(this.ephemeralKeypair.getPrivate(), packet.getPublicKey());

      aesCiphers = DecentralizedAuthNetworkEncryptionUtils.deriveAESCiphers(this.sharedSecret);

      responsePacket = new DecentralizedAuthEncryptionResponseC2SPacket(this.ephemeralKeypair.getPublic());
    }
    catch (NetworkEncryptionException e) {
      throw new IllegalStateException("Protocol error", e);
    }
    this.statusConsumer.accept(Text.translatable("connect.authorizing"));
    NetworkUtils.EXECUTOR.submit(() -> {
      this.statusConsumer.accept(Text.translatable("connect.encrypting"));
      this.connection.send(responsePacket, future -> this.connection.setupEncryption(aesCiphers.decryptCipher(), aesCiphers.encryptCipher()));
    });
  }

  @Override
  public void onAuthChallenge(DecentralizedAuthAuthChallengeS2CPacket packet) {
    Ed25519PublicKeyParameters serverIdentityPublicKey = packet.getServerIdentityPublicKey();
    // TODO cache the server's identity or compare it to the cached version from the previous connection
    DecentralizedAuth.LOGGER.info("Server claims identity: " + Base64.getEncoder().encodeToString(serverIdentityPublicKey.getEncoded()));
    if (!packet.verifySignedSharedSecret(this.sharedSecret)) {
      this.connection.disconnect(Text.of("Failed to validate server identity!"));
      return;
    }

    Ed25519PrivateKeyParameters clientIdentityPrivateKey = ((MinecraftClientAccess) this.client).getDecentralizedAuthIdentity();
    Ed25519PublicKeyParameters clientIdentityPublicKey = clientIdentityPrivateKey.generatePublicKey();

    byte[] signedSharedSecret = DecentralizedAuthNetworkEncryptionUtils.generateIdentityProof(clientIdentityPrivateKey, this.sharedSecret);

    this.connection.send(new DecentralizedAuthAuthProofC2SPacket(clientIdentityPublicKey, signedSharedSecret));
  }

  @Override
  public void onProfileRequest(DecentralizedAuthProfileRequestS2CPacket packet) {
    // TODO skin texture
    this.connection.send(new DecentralizedAuthProfileResponseC2SPacket(this.client.getSession().getUsername()));
  }
}
