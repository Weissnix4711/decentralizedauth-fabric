package net.decentralizedauth.fabric.packet;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;

public class DecentralizedAuthDisconnectS2CPacket implements Packet<ClientDecentralizedAuthPacketListener> {
  private final Text reason;

  public DecentralizedAuthDisconnectS2CPacket(Text reason) {
    this.reason = reason;
  }

  public DecentralizedAuthDisconnectS2CPacket(PacketByteBuf buf) {
    this.reason = Text.Serializer.fromLenientJson(buf.readString(PacketByteBuf.MAX_TEXT_LENGTH));
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeText(this.reason);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onDisconnect(this);
  }

  public Text getReason() {
    return this.reason;
  }
}
