package net.decentralizedauth.fabric.packet;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthProfileRequestS2CPacket
implements Packet<ClientDecentralizedAuthPacketListener> {
  private final boolean includeSkinTexture;

  public DecentralizedAuthProfileRequestS2CPacket(boolean includeSkinTexture) {
    this.includeSkinTexture = includeSkinTexture;
  }

  public DecentralizedAuthProfileRequestS2CPacket(PacketByteBuf buf) {
    this.includeSkinTexture = buf.readBoolean();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeBoolean(this.includeSkinTexture);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onProfileRequest(this);
  }

  public boolean getIncludeSkinTexture() {
    return this.includeSkinTexture;
  }
}
