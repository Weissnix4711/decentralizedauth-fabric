package net.decentralizedauth.fabric.packet;

import javax.crypto.SecretKey;

import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthAuthProofC2SPacket
implements Packet<ServerDecentralizedAuthPacketListener> {
  private final byte[] clientIdentityPublicKey;
  private final byte[] signedSharedSecret;

  public DecentralizedAuthAuthProofC2SPacket(Ed25519PublicKeyParameters clientIdentityPublicKey, byte[] signedSharedSecret) {
    this.clientIdentityPublicKey = clientIdentityPublicKey.getEncoded();
    this.signedSharedSecret = signedSharedSecret;
  }

  public DecentralizedAuthAuthProofC2SPacket(PacketByteBuf buf) {
    this.clientIdentityPublicKey = buf.readByteArray();
    this.signedSharedSecret = buf.readByteArray();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.clientIdentityPublicKey);
    buf.writeByteArray(this.signedSharedSecret);
  }

  @Override
  public void apply(ServerDecentralizedAuthPacketListener serverLoginPacketListener) {
    serverLoginPacketListener.onAuthProof(this);
  }

  public Ed25519PublicKeyParameters getClientIdentityPublicKey() {
    return new Ed25519PublicKeyParameters(this.clientIdentityPublicKey, 0);
  }

  public boolean verifySignedSharedSecret(SecretKey sharedSecret) {
    return DecentralizedAuthNetworkEncryptionUtils.verifyIdentityProof(this.getClientIdentityPublicKey(), this.signedSharedSecret, sharedSecret);
  }
}
