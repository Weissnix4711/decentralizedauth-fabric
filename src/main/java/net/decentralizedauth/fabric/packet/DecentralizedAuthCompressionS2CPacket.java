package net.decentralizedauth.fabric.packet;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthCompressionS2CPacket
implements Packet<ClientDecentralizedAuthPacketListener> {
  private final int compressionThreshold;

  public DecentralizedAuthCompressionS2CPacket(int compressionThreshold) {
    this.compressionThreshold = compressionThreshold;
  }

  public DecentralizedAuthCompressionS2CPacket(PacketByteBuf buf) {
    this.compressionThreshold = buf.readVarInt();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeVarInt(this.compressionThreshold);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onCompression(this);
  }

  public int getCompressionThreshold() {
    return this.compressionThreshold;
  }
}
