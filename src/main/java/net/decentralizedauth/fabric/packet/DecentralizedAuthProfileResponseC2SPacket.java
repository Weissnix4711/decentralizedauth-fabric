package net.decentralizedauth.fabric.packet;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

// TODO support skin texture
public record DecentralizedAuthProfileResponseC2SPacket(String name) implements Packet<ServerDecentralizedAuthPacketListener> {
  public DecentralizedAuthProfileResponseC2SPacket(PacketByteBuf buf) {
    this(buf.readString(16));
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeString(this.name, 16);
  }

  @Override
  public void apply(ServerDecentralizedAuthPacketListener serverDecentralizedAuthPacketListener) {
    serverDecentralizedAuthPacketListener.onProfileResponse(this);
  }
}
