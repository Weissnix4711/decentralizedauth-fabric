package net.decentralizedauth.fabric.mixin.server;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.chocohead.mm.api.ClassTinkerers;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthNetworkHandler;
import net.minecraft.SharedConstants;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkState;
import net.minecraft.network.packet.c2s.handshake.HandshakeC2SPacket;
import net.minecraft.network.packet.s2c.login.LoginDisconnectS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerHandshakeNetworkHandler;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;

@Mixin(ServerHandshakeNetworkHandler.class)
class ServerHandshakeNetworkHandlerMixin {
  @Shadow
  private MinecraftServer server;
  @Shadow
  private ClientConnection connection;

  @Inject(at = @At("HEAD"), method = "onHandshake(Lnet/minecraft/network/packet/c2s/handshake/HandshakeC2SPacket;)V", cancellable = true)
  public void onOnHandshake(HandshakeC2SPacket packet, CallbackInfo info) {
    NetworkState decentralizedAuth = ClassTinkerers.getEnum(NetworkState.class, "DECENTRALIZEDAUTH");
    if (packet.getIntendedState() == decentralizedAuth) {
      this.connection.setState(decentralizedAuth);
      if (packet.getProtocolVersion() != SharedConstants.getGameVersion().getProtocolVersion()) {
        MutableText text = packet.getProtocolVersion() < 754 ? Text.translatable("multiplayer.disconnect.outdated_client", SharedConstants.getGameVersion().getName()) : Text.translatable("multiplayer.disconnect.incompatible", SharedConstants.getGameVersion().getName());
        this.connection.send(new LoginDisconnectS2CPacket(text));
        this.connection.disconnect(text);
        info.cancel();
      }
      this.connection.setPacketListener(new ServerDecentralizedAuthNetworkHandler(this.server, this.connection));
      info.cancel();
    }
  }
}
