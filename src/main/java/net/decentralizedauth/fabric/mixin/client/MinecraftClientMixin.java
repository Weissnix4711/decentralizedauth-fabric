package net.decentralizedauth.fabric.mixin.client;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.Optional;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftClientAccess;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.RunArgs;

@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMixin implements MinecraftClientAccess {
  @Shadow
  public File runDirectory;

  @Nullable
  @Unique
  private Ed25519PrivateKeyParameters id;

  @Override
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id) {
    this.id = id;
  }

  @Override
  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity() {
    return this.id;
  }

  private void initializeDecentralizedAuthIdentity() {
    File identityFile = new File(this.runDirectory, "id_ed25519");
    Optional<File> optional_keypair_path = Optional.of(identityFile).filter(File::isFile);
    Optional<Ed25519PrivateKeyParameters> optional_keypair = optional_keypair_path.map(file -> {
      try {
        byte[] keyData = Files.readAllBytes(file.toPath());
        Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(keyData);
        return privateKey;
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      return null;
    });
    if (!optional_keypair.isPresent()) {
      DecentralizedAuth.LOGGER.info("No usable Decentralized Auth identity found, generating a new keypair for you!");
      SecureRandom random = new SecureRandom();
      Ed25519PrivateKeyParameters pk = new Ed25519PrivateKeyParameters(random);
      byte[] keyData = pk.getEncoded();
      try {
        Files.write(identityFile.toPath(), keyData);
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      DecentralizedAuth.LOGGER.info("Your new Decentralized Auth identity was written to `id_ed25519`.");
      optional_keypair = Optional.of(pk);
    }

    this.setDecentralizedAuthIdentity(optional_keypair.get());
    DecentralizedAuth.LOGGER.info("Decentralized Auth is ready to accept new connections");
  }

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/client/MinecraftClient.setOverlay(Lnet/minecraft/client/gui/screen/Overlay;)V"), method = "<init>(Lnet/minecraft/client/RunArgs;)V")
  protected void runServerMixin(RunArgs args, CallbackInfo info) {
    DecentralizedAuth.LOGGER.info("Loading Decentralized Auth identity...");
    this.initializeDecentralizedAuthIdentity();
  }
}
