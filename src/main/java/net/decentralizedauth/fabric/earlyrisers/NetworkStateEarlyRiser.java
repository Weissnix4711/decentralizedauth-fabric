package net.decentralizedauth.fabric.earlyrisers;

import com.chocohead.mm.api.ClassTinkerers;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.MappingResolver;
import net.minecraft.network.NetworkState;

public class NetworkStateEarlyRiser implements Runnable {
  @Override
  public void run() {
    MappingResolver remapper = FabricLoader.getInstance().getMappingResolver();

    String networkStateEnum = remapper.mapClassName("intermediary", "net.minecraft.network.NetworkState");
    String networkStatePacketHandlerInitializerClass = "L" + remapper.mapClassName("intermediary", "net.minecraft.network.NetworkState$PacketHandlerInitializer") + ";";
    ClassTinkerers.enumBuilder(networkStateEnum, int.class, networkStatePacketHandlerInitializerClass)
      .addEnum("DECENTRALIZEDAUTH", this::createDecentralizedAuthEnumVariant)
      .build();
  }

  Object[] createDecentralizedAuthEnumVariant() {
    return new Object[] {
      69,
      // We provide an empty PacketHandlerInitializer here and call `setup` in
      // the `NetworkState` constructor to avoid re-entrance issues.
      new NetworkState.PacketHandlerInitializer()
    };
  }
}
