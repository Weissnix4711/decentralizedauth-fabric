package net.decentralizedauth.fabric.access;

import java.util.Optional;

import com.mojang.authlib.GameProfile;

public interface UserCacheAccess {
  /**
   * Similar to `UserCache.findByName`, but doesn't fallback to Mojang's
   * servers if nothing is found locally.
   */
  public Optional<GameProfile> findCachedByName(String name);
}
