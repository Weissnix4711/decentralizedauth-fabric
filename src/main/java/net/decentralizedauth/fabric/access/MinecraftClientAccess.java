package net.decentralizedauth.fabric.access;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.jetbrains.annotations.Nullable;

public interface MinecraftClientAccess {
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id);

  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity();
}
