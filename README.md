# Minecraft Decentralized Auth

Securely manage registered Minecraft or Minecraft-compatible client connections without a connection to Mojang or Microsoft.
Own your infrastructure.

## Building

Set up your environment according to [fabric wiki page](https://fabricmc.net/wiki/tutorial:setup).

Building from the command line can be done using `gradle assemble`.
The resulting jar file works on both the client and server and can be found in `build/libs/decentralizedauth-<VERSION>.jar`.

## Testing

The `configureLaunch` and `configureClientLaunch` Gradle tasks can be used to set up a local environment for the server and client respectively.
Once configured, the `runServer` and `runClient` Gradle tasks will build your latest changes and spin up a new server or client respectively with the mod installed.

## License

This software and its associated documentation are released into the public domain by the copyright holders.
